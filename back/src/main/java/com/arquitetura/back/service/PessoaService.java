package com.arquitetura.back.service;

import com.arquitetura.back.entity.Pessoa;
import com.arquitetura.back.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PessoaService {

    @Autowired
    PessoaRepository repository;

    public List<Pessoa> findAll(){
        return repository.findAll();
    }

    public Pessoa findById(long id){
        return repository.findById(id).orElse(null);
    }

    public Pessoa save(Pessoa pessoa){
        return repository.save(pessoa);
    }

    public Pessoa update(Pessoa pessoa){
        Pessoa pessoaBanco = this.findById(pessoa.getId());
        if(pessoaBanco != null){
            return repository.save(pessoa);
        } else {
            return null;
        }
    }

    public boolean delete(long id){
        Pessoa pessoa = this.findById(id);
        if(pessoa != null){
            repository.delete(pessoa);
            return true;
        } else {
            return false;
        }
    }

}
